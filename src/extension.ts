'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as prefix from './prefix';
import * as config from './config';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    let idGenerator = new IDGenerator();
    let conf = new config.Config();

    let createErrID = vscode.commands.registerCommand('extension.createID', () => {
        const editor = vscode.window.activeTextEditor;
        if (editor == undefined) {
            return;
        }

        const config = conf.GetConfig(editor);
        if (config == undefined) {
            return;
        }
       
        editor.edit(editBuilder => {
            editBuilder.insert(editor.selection.active, idGenerator.createID(config));
        });
    });

    let createIDWorkspace = vscode.commands.registerCommand('extension.createIDWorkspace', () => {
        const editor = vscode.window.activeTextEditor;
        if (editor == undefined) {
            return;
        }

        const config = conf.GetConfig(editor); 
        if (config == undefined) {
            return;
        }
        editor.edit(editBuilder => {
            editBuilder.insert(editor.selection.active, idGenerator.createIDWorkspace(editor, config));
        });
    });

    let createIDSetPrefix = vscode.commands.registerCommand('extension.createIDSetPrefix', async () => {
        const editor = vscode.window.activeTextEditor;
        if (editor === undefined) {
            return;
        }

        const config = conf.GetConfig(editor);
        if (config === undefined) {
            return;
        }

        const code = await idGenerator.createIDPrefixFromInput(config);
        if (code === undefined) {
            return;
        }

        editor.edit(editBuilder => {
            editBuilder.insert(editor.selection.active, code);
        })
    });

    context.subscriptions.push(createIDWorkspace);
    context.subscriptions.push(createErrID);
    context.subscriptions.push(createIDSetPrefix);
}

class IDGenerator {
    private possibilitiesName = 'Possibilities';
    private codeLenName = 'SuffixLength';
    private prefixGenerator: prefix.Prefix = new prefix.Prefix;

    private getOrDefault<T>(toCheck : T | undefined, d : T) : T {
        if (toCheck !== undefined) {
            return toCheck;
        }
        return d;
    }

    public createID(config : vscode.WorkspaceConfiguration) {
        return this.prefixGenerator.fromContext(config) + this.createText(config);
    }

    public createIDWorkspace(editor: vscode.TextEditor,  config: vscode.WorkspaceConfiguration) : string{
        return this.prefixGenerator.workspacePrefix(editor, config) + "-" + this.createText(config);
    }

    public async createIDPrefixFromInput(config : vscode.WorkspaceConfiguration): Promise<string | undefined> {
        let prefix = await this.prefixGenerator.fromInput();
        if (prefix === undefined) {
            return prefix;
        }

        let txt = this.createText(config);

        if (prefix === "") {
            return txt;
        }
        return prefix.toUpperCase() + "-" + txt;
    }

    private createText(config : vscode.WorkspaceConfiguration) : string {
        var text = ""; 

        var possibilities = this.getOrDefault<string>(config.get<string>(this.possibilitiesName), "");
        var length = this.getOrDefault<number>(config.get<number>(this.codeLenName), 0);

        for (var i = 0; i < length; i++) {
            text += possibilities.charAt(Math.floor(Math.random() * possibilities.length));
        }
        return text;
    }

    // TODO use function
    // function checkID(prefix: string) {
    //     //TODO get all errorID's from project and check if ID exists
    //     return true
    // }
}
    // this method is called when your extension is deactivated
    export function deactivate() {}
